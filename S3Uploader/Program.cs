﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Helpers;
using NLog;
using S3Uploader.DB.Pepa;

namespace S3Uploader
{
    class Program
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();
        private static string BUCKET_NAME = ConfigurationManager.AppSettings["S3BucketName"];
        private static RegionEndpoint REGION = RegionEndpoint.GetBySystemName(ConfigurationManager.AppSettings["S3RegionName"]);
        static void Main(string[] args)
        {
            var accessKeys = AwsCredentialsLoader.LoadCredentials();

            AmazonS3Config config = new AmazonS3Config()
            {
                SignatureVersion = "v4",
                RegionEndpoint = REGION
            };
            AWSCredentials creds = new BasicAWSCredentials(accessKeys.AWSAccessKey, accessKeys.AWSSecretKey);

            AmazonS3Client client = new AmazonS3Client(creds, config);
            ListBucketsResponse response = client.ListBuckets();
            if (response.Buckets.All(x => !x.BucketName.Equals(BUCKET_NAME, StringComparison.OrdinalIgnoreCase)))
            {
                PutBucketRequest createBucketRequest = new PutBucketRequest();
                createBucketRequest.BucketName = BUCKET_NAME;
                client.PutBucket(createBucketRequest);
            }


 
            using (var ctx = new PepaDBEntities())
            {
                var documents = ctx.Documents.ToList();
                documents.ForEach((doc) =>
                {
                    var docId = Guid.NewGuid().ToString();
                    var repoPath = $"{GenerateHash()}-{DateTime.Now:dd-MM-yyyy}/";
                    var docPath = $"{docId}{doc.DocType}";
                    var fullPath = repoPath + docPath;
                    PutObjectRequest request = new PutObjectRequest
                    {
                        BucketName = BUCKET_NAME,
                        Key = fullPath,
                        CannedACL = S3CannedACL.Private,
                        ServerSideEncryptionMethod = ServerSideEncryptionMethod.AWSKMS,


                    };

                    try
                    {
                        using (var ms = new MemoryStream(doc.DocImage))
                        {
                            request.InputStream = ms;
                            var putResponse = client.PutObject(request);
                            if (putResponse.HttpStatusCode == HttpStatusCode.OK)
                            {
                                doc.RepositoryFolder = BUCKET_NAME;
                                doc.RepositoryDocumentIdentifer = docId;
                                doc.RepositoryRegion = REGION.ToString();
                                doc.RepositoryPath = repoPath;
                                doc.RepositoryProvider = "AwsS3";
                                ctx.SaveChanges();
                            }

                        }
                        

                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, $"Upload failed for file {doc.DocumentId} for path {fullPath}");
                    }

                });

            }

            Console.WriteLine("Finished.");
            Console.ReadKey();
        }


        static string GenerateHash()
        {
            var guid = Guid.NewGuid().ToString();
            return guid.Substring(0, guid.IndexOf('-'));
        }
    }
}
