﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Helpers
{
    public static class AwsCredentialsLoader
    {
        public static AwsCredentials LoadCredentials()
        {
            using (StreamReader r = new StreamReader("aws-creds.json"))
            {
                string json = r.ReadToEnd();
                AwsCredentials items = JsonConvert.DeserializeObject<AwsCredentials>(json);
                return items;
            }
        }
    }

    public sealed class AwsCredentials
    {
        public string AWSAccessKey { get; set; }
        public string AWSSecretKey { get; set; }
    }
}
