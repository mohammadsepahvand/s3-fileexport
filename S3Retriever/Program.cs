﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Helpers;

namespace S3Retriever
{
    class GeneratePresignedUrl
    {
        
        private static string BUCKET_NAME = ConfigurationManager.AppSettings["S3BucketName"];
        private static readonly RegionEndpoint _region = RegionEndpoint.GetBySystemName(ConfigurationManager.AppSettings["S3RegionName"]);
        private static readonly AwsCredentials AccessKeys = AwsCredentialsLoader.LoadCredentials();


        public static void Main(string[] args)
        {
            
            AWSConfigs.S3Config.UseSignatureVersion4 = true;
            var config = new AmazonS3Config()
            {
                SignatureVersion = AWSConfigs.S3UseSignatureVersion4Key,

                RegionEndpoint = _region
            };
            using (var s3Client = new AmazonS3Client(new BasicAWSCredentials(AccessKeys.AWSAccessKey, AccessKeys.AWSSecretKey), config))
            {
                string urlString = GeneratePreSignedUrl(s3Client, 5);
                Console.WriteLine(urlString);
            }


            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        static string GeneratePreSignedUrl(IAmazonS3 client, int minutesToExpire)
        {
            string urlString = "";
            var key = ListFiles.GetObjects(BUCKET_NAME, AccessKeys.AWSAccessKey, AccessKeys.AWSSecretKey, _region).ElementAt(20);
            GetPreSignedUrlRequest request1 = new GetPreSignedUrlRequest
            {
                BucketName = BUCKET_NAME,
                Key = key,
                Expires = DateTime.Now.AddMinutes(minutesToExpire),


            };

            try
            {
                urlString = client.GetPreSignedURL(request1);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {
                    Console.WriteLine("Check the provided AWS Credentials.");
                    Console.WriteLine(
                    "To sign up for service, go to http://aws.amazon.com/s3");
                }
                else
                {
                    Console.WriteLine(
                     "Error occurred. Message:'{0}' when listing objects",
                     amazonS3Exception.Message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return urlString;

        }
    }

    static class ListFiles
    {
        public static List<string> GetObjects(string BUCKET_NAME, string ACCESSKEY, string SECRETACCESSKEY, RegionEndpoint region)
        {
            AmazonS3Client client = new AmazonS3Client(new BasicAWSCredentials(ACCESSKEY, SECRETACCESSKEY), new AmazonS3Config()
            {
                RegionEndpoint = region,
                SignatureVersion = AWSConfigs.S3UseSignatureVersion4Key
            });


            ListObjectsRequest request = new ListObjectsRequest()
            {

            };
            request.BucketName = BUCKET_NAME;
            do
            {
                ListObjectsResponse response = client.ListObjects(request);

                if (response.IsTruncated)
                {
                    request.Marker = response.NextMarker;
                }
                else
                {
                    request = null;
                }
                return response.S3Objects.Select(x => x.Key).ToList();
            } while (request != null);


        }
    }
}
